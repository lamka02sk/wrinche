<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     * @return void
     */
    public function map() {

        $installed = $this->app->config['app']['installed'];

        if($installed) {
            $this->mapAdminRoutes();
            $this->mapApiRoutes();
            $this->mapWebRoutes();
        } else {
            $this->mapInstallRoutes();
        }

    }

    /**
     * Define routes from given path
     * @return void
     */
    private function mapRoutesInPath(string $routesPath, string $routesPrefix = '/') {


        $path = 'routes/' . $routesPath;
        $folder = scandir(base_path($path));
        $folder = array_diff($folder, ['.', '..']);

        foreach($folder as $file) {

            $filePath = base_path($path . '/' . $file);

            // Check if item is really a file
            if(!is_file($filePath)) {
                continue;
            }

            $fileName = explode('.', $file);
            array_pop($fileName);
            $fileName = join('-', $fileName);

            Route::middleware($routesPath, 'throttle:20,1')
                ->name($routesPath . ($file === 'main.php' ? '' : ('.' . $fileName)))
                ->prefix($routesPrefix)
                ->namespace($this->namespace . '\\' . ucfirst($routesPath))
                ->group($filePath);

        }

    }

    /**
     * Define the "admin" routes for the application.
     * @return void
     */
    protected function mapAdminRoutes() {
        $adminPrefix = '';
        $this->mapRoutesInPath('admin', $adminPrefix);
    }

    /**
     * Define the "web" routes for the application.
     * @return void
     */
    protected function mapWebRoutes() {
        $this->mapRoutesInPath('web');
    }

    /**
     * Define the "api" routes for the application.
     * @return void
     */
    protected function mapApiRoutes() {
        $this->mapRoutesInPath('api', 'api');
    }

    /**
     * Define the "install" routes for the application.
     * @return void
     */
    protected function mapInstallRoutes() {
        $this->mapRoutesInPath('install');
    }

}
