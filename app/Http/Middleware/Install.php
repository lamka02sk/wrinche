<?php

namespace App\Http\Middleware;

use Closure;

class Install {

    protected $symlinks = [
        ['/storage/app/public', '/public/storage'],
        ['/storage/data', '/public/data']
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $this->createSymlinks();
        return $next($request);
    }

    /**
     * Create symlinks to public storage
     *
     * @return void
     */
    protected function createSymlinks() {

        $basePath = base_path();
        foreach($this->symlinks as $symlink) {
            if(is_link($basePath . $symlink[1])) continue;
            if(!symlink($basePath . $symlink[0], $basePath . $symlink[1])) {
                abort(403, 'Forbidden');
            }
        }

    }

}
