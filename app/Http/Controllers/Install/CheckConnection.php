<?php

namespace App\Http\Controllers\Install;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckConnection extends Controller {

    protected $credentials;

    public function __construct(Request $request) {
        $this->credentials = $request->all();
    }

    public function index() : object {

        $validator = Validator::make($this->credentials, [
            'host' => 'required|filled',
            'port' => ['required', 'filled', 'regex:/^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/'],
            'database' => 'required|filled',
            'user' => 'required|filled',
            'password' => 'present'
        ]);

        if($validator->fails())
            return response()->json([
                'success' => false,
                'code' => 200,
                'message' => 'Credentials invalid'
            ]);

        $success = $this->checkConnection();

        return response()->json([
            'success' => $success,
            'code' => 200,
            'message' => $success ? 'Credentials confirmed' : 'Credentials are not correct'
        ]);

    }

    protected function checkConnection() : bool {

        $dsn = "mysql:host=" . $this->credentials['host'] . ";dbname=" . $this->credentials['database'];

        try {
            new \PDO($dsn, $this->credentials['user'], $this->credentials['password']);
            return true;
        } catch(\Exception $e) {
            return false;
        }

    }

}
