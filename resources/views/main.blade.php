<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="h-full m-none w-full">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>@yield('title') - {{ __('system.name') }}. {{ __('system.description') }}</title>
        <link rel="icon" href="/img/logo/favicon.png" type="image/png">
        @yield('css')
        <link rel="stylesheet" href="/fonts/fontawesome/css/all.min.css" type="text/css">
        @yield('js-head')
    </head>
    <body class="h-full m-none w-full">
        <noscript>
            This website requires JavaScript in your browser to be enabled.
        </noscript>
        <div id="app" class="w-full h-full">
            @yield('body')
        </div>
        @yield('js')
    </body>
</html>
