@extends('errors.main')

@section('title', 503)
@section('description', __('errors.503'))

@section('description-long')
    We are working hard to make our website better. It will not last long, we will be right back. See you soon!
@endsection
