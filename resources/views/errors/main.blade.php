<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Error @yield('title') - wrinche</title>
        <link rel="icon" href="/img/errors/error.png" type="image/png">
        <style>
            html, body {
                width: 100%;
                height: 100%;
                margin: 0;
                font-family: 'Inter UI', sans-serif;
            }
            .panel {
                display: block;
                width: 40%;
                height: 100%;
                float: left;
                overflow: hidden;
                position: relative;
            }
            .panel.left-panel {
                display: flex;
                flex-direction: column;
                justify-content: center;
                padding: 100px;
                box-sizing: border-box;
            }
            .panel.left-panel a {
                color: rgb(0, 132, 255);
                font-weight: 600;
                text-decoration: none;
                margin-bottom: 6px;
            }
            .panel.left-panel img {
                width: 200px;
                height: auto;
                margin-bottom: 64px;
            }
            .panel.right-panel {
                background: url('/img/errors/@yield("title").jpg') center no-repeat;
                background-size: cover;
                width: 60%;
            }
            .panel.panel.right-panel .credits {
                position: absolute;
                bottom: 18px;
                left: 18px;
                margin: 0;
                font-size: .85rem;
                color: whitesmoke;
                text-shadow: 0 2px 2px grey, 0 -2px 2px grey, 2px 0px 2px grey, -2px 0px 2px grey
            }
            h1 {
                font-size: 6.2rem;
                font-weight: 600;
                margin: 0;
                color: rgb(36, 36, 36);
            }
            h2 {
                font-size: 2.6rem;
                font-weight: 400;
                color: rgb(36, 36, 36);
                margin: 0;
            }
        </style>
    </head>
    <body>
        <div class="panel left-panel">
            <a href="/">Go back home</a>
            <img src="/img/logo/dark.png" alt="wrinche">
            <h1>@yield('title')</h1>
            <h2>@yield('description')</h2>
            <p>@yield('description-long')</p>
        </div>
        <div class="panel right-panel">
            <p class="credits">
                Photo from Unsplash
            </p>
        </div>
    </body>
</html>
