@extends('errors.main')

@section('title', 403)
@section('description', __('errors.403'))

@section('description-long')
    You are not allowed to access this page. Please return back to the
    <a href="/">homepage</a>.
@endsection
