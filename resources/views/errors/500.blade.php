@extends('errors.main')

@section('title', 500)
@section('description', __('errors.500'))

@section('description-long')
    We are out. Something went horribly wrong on this page, but we are gonna fix it. In the meantime, please continue to our
    <a href="/">homepage</a>.
@endsection
