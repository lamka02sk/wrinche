@extends('errors.main')

@section('title', 401)
@section('description', __('errors.401'))

@section('description-long')
    You are not authorized to view this page. Please return back to the
    <a href="/">homepage</a>.
@endsection
