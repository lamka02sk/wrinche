@extends('errors.main')

@section('title', 402)
@section('description', __('errors.402'))

@section('description-long')
    This page is only for users with premium membership. Return to
    <a href="/">homepage</a>.
@endsection
