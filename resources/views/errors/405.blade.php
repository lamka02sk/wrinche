@extends('errors.main')

@section('title', 405)
@section('description', __('errors.405'))

@section('description-long')
    Requested method is not valid for current route. Please, check the request and try again, or visit our
    <a href="/">homepage</a>.
@endsection
