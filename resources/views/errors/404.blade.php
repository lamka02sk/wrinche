@extends('errors.main')

@section('title', 404)
@section('description', __('errors.404'))

@section('description-long')
    The page you are looking for could not be found on this server. Please, check the URL and try again, or visit our
    <a href="/">homepage</a>.
@endsection