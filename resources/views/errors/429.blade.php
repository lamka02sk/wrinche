@extends('errors.main')

@section('title', 429)
@section('description', __('errors.429'))

@section('description-long')
    Whooah! Stop it, too much to handle, don't you think?!
@endsection
