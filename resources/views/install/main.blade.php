@extends('./../main')
@section('title', __('install/general.title'))

@section('css')
    <link rel="stylesheet" href="{{ mix('css/install/app.css') }}" type="text/css">
@endsection

@section('js')
    <script src="{{ mix('js/install/app.js') }}"></script>
@endsection

@section('body')

    <div class="center-wrapper min-h-full min-w-full flex flex-col justify-center items-center">

        <div class="content-wrapper flex flex-col justify-center items-center p-lg">
            <header class="header mb-lg">
                <h1 class="hidden">{{ __('system.name') }}</h1>
                <img src="/img/logo/olive.png" alt="{{ __('system.name') }}">
            </header>
            <p class="welcome text-center text-lg text-dark" :class="classes">
                <b>@{{ $t('installer.welcome') }}</b>
                @{{ $t('installer.thank_you') }}<br>@{{ $t('installer.few_things') }}
            </p>
        </div>

        <div id="steps" class="steps-hidden" style="height: 0">
            <transition name="slide-left" mode="out-in">
                <router-view ref="step" :step="step"></router-view>
            </transition>
        </div>

        <div class="spacer"></div>

        <div class="connection-test hide">
            <button class="button button-secondary button-sm mb-default test-connection" @click="testConnection">
                @{{ $t('installer.connection.test') }}
            </button>
            <span v-if="DBTest !== null" :class="DBTest === false ? 'connection-invalid' : 'connection-valid'">
                @{{ DBTest === false ? $t('installer.connection.fail') : $t('installer.connection.success') }}
            </span>
        </div>


        <button class="button button-primary next-step" :class="classes" v-if="(+step + 1) !== steps.length" @click="nextStep">
            @{{ $t('installer.next') }} <i class="fa fa-chevron-right text-sm"></i>
        </button>

        <button class="button button-primary next-step" :class="classes" v-else @click="install">
            @{{ $t('installer.install') }} <i class="fa fa-chevron-right text-sm"></i>
        </button>

    </div>

@endsection
