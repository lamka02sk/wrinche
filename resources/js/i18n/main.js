import axios from 'axios';
import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const messages = {};
const loadedLanguages = {};
const defaultLanguage = 'en';

export const i18n = new VueI18n({
    locale: 'en',
    domain: null,
    messages,
    silentTranslationWarn: true,
});

function setI18nLanguage(lang, domain) {
    i18n.locale = lang;
    i18n.domain = domain;
    axios.defaults.headers.common['Accept-Language'] = lang;
    document.querySelector('html').setAttribute('lang', lang);
    return lang;
}

export function loadLanguageAsync(lang, domain) {

    if(i18n.locale !== lang || i18n.domain !== domain) {

        if(!loadedLanguages[lang] || !loadLanguages[lang][domain]) {
            return import(/* webpackChunkName: "/i18n/[request]" */ `./lang/${domain.replace('.', '/')}/${lang}`).then(result => {

                i18n.setLocaleMessage(lang, {
                    ...messages[lang],
                    [domain]: result.default
                });

                if(!loadLanguages[domain]) loadedLanguages[domain] = [];
                loadLanguages[domain].push(lang);

                return setI18nLanguage(lang, domain);

            }).catch(() => {
                if(lang !== defaultLanguage)
                    return loadLanguageAsync(defaultLanguage, domain);
            });
        }

        return Promise.resolve(setI18nLanguage(lang, domain));

    }

    return Promise.resolve(lang);

}
