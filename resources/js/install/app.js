require('./../bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './router';
import axios from 'axios';
import { i18n, loadLanguageAsync } from './../i18n/main';

Vue.use(VueRouter);

window.vm = new Vue({
    el: '#app',
    router,
    i18n,
    data: {
        classes: [],
        installRoute: 'install',
        forms: [],
        DBTest: null,
        step: 0, // current step
        steps: ['interface', 'account', 'database', 'website'],
    },
    beforeCreate() {
        loadLanguageAsync('en', 'installer');
    },
    mounted() {
        setTimeout(this.recalculateHeight, 5250);
    },
    methods: {
        setFormData(formIndex, data) {
            this.forms[formIndex] = data;
        },
        setFixedHeight() {
            const stepsWrapper = document.querySelector('#steps');
            stepsWrapper.style.height = stepsWrapper.offsetHeight + 'px';
        },
        prevStep() {

            this.step = --this.step >= 0 ? this.step : 0;
            this.$router.push({ name: this.steps[this.step] });

            this.setFixedHeight();
            this.recalculateHeightDelay();

        },
        nextStep() {

            this.step = ++this.step < this.steps.length ? this.step : null;

            if(!this.step) {
                return false;
            }

            // Next step
            this.$router.push({ name: this.steps[this.step] });
            this.setFixedHeight();
            this.recalculateHeightDelay();

        },
        install() {

            // Hide button and welcome text
            this.classes.push('installing');

            // Redirect to installation component
            this.$router.push({ name: this.installRoute });
            this.setFixedHeight();
            this.recalculateHeightDelay();

        },
        recalculateHeightDelay(delay = 600) {
            setTimeout(this.recalculateHeight, delay);
        },
        recalculateHeight() {

            let autoHeight, currentHeight;
            const stepsWrapper = document.querySelector('#steps');

            stepsWrapper.classList.remove('visible-overflow');
            currentHeight = stepsWrapper.offsetHeight + 'px';
            stepsWrapper.style.height = 'auto';
            autoHeight = stepsWrapper.offsetHeight;
            stepsWrapper.style.height = currentHeight;

            setTimeout(() => {

                stepsWrapper.classList.remove('steps-hidden');
                stepsWrapper.style.height = autoHeight + 'px';

                setTimeout(() =>  {
                    stepsWrapper.classList.add('visible-overflow');
                    stepsWrapper.style.height = 'auto';
                }, 500);

            }, 50);

        },
        async testConnection() {

            if(!this.forms[2]) {
                this.DBTest = false;
                return false;
            }

            const result = (await axios.post('/check-connection', this.forms[2])).data;
            this.DBTest = !!result.success;

            return this.DBTest;

        },
        firstStep() {
            this.classes = [];
            this.forms = [];
            this.DBTest = null;
            this.step = 0;
            this.$router.push({ name: this.steps[0] });
        }
    }
});
