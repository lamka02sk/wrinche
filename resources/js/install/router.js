import VueRouter from 'vue-router'
import Interface from './components/Interface';
import Account from './components/Account';
import Database from './components/Database';
import Website from './components/Website';
import Install from './components/Install';
import Installed from './components/Installed';

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'interface',
            component: Interface,
            meta: {
                step: 0
            }
        },
        {
            path: '/account',
            name: 'account',
            component: Account,
            meta: {
                step: 1
            }
        },
        {
            path: '/database',
            name: 'database',
            component: Database,
            meta: {
                step: 2
            }
        },
        {
            path: '/website',
            name: 'website',
            component: Website,
            meta: {
                step: 3
            }
        },
        {
            path: '/install',
            name: 'install',
            component: Install
        },
        {
            path: '/installed',
            name: 'installed',
            component: Installed,
            props: true,
        }
    ],

});
