export default class {

    constructor(data, prototype, component = null) {

        this.data = data;
        this.prototype = prototype;
        this.component = component;

        this.validate();

    }

    validate() {
        Object.keys(this.data).forEach(field => {
            this.validateField(field);
        });
    }

    validateField(field) {

        if(!this.prototype.hasOwnProperty(field))
            return true;

        const errors = [];
        Object.keys(this.prototype[field].rules).every(rule => {
            const error = this.validateRule(field, rule);
            errors.push(error);
            return error === null;
        });

        const error = errors.pop();
        this.prototype[field].message = error;
        this.prototype[field].valid = error === null;

    }

    validateRule(field, rule) {

        const fieldValue = this.data[field];
        const ruleName = rule;
        rule = this.prototype[field].rules[rule];

        return this['check_' + ruleName](fieldValue, rule);

    }

    check_length(value, config) {

        const length = value.length;
        return length < config[0]
            ? 'LEN_SHORT'
            : (length > config[1]
                ? 'LEN_LONG'
                : null);

    }

    check_regex(value, regex) {
        return value.match(regex) ? null : 'REGEX';
    }

    check_match(value, against) {
        return value === this.data[against] ? null : 'MATCH';
    }

    check_key(key, object) {
        return this.component[object].hasOwnProperty(key) ? null : 'HAS_KEY';
    }

    check_equals(value, compare) {
        return value === compare ? null : 'EQUALS';
    }

    check_empty(value, result) {
        return result
            ? (value.trim().length < 1 ? null : 'IS_FILLED')
            : (value.trim().length < 1 ? 'IS_EMPTY' : null);
    }

}
