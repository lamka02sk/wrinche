<?php

return [

    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    429 => 'Too Many Requests',

    500 => 'Internal Server Error',
    503 => 'Service Unavailable'

];
