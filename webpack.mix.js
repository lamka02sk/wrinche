const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/install/app.sass', 'public/css/install')
    .options({
        postCssUrls: false,
        postCss: [
            tailwindcss('resources/js/tailwind/config.js')
        ],
        clearConsole: true,
    });

mix.js('resources/js/install/app.js', 'public/js/install')
   .webpackConfig({
      module: {
         rules: [
            {
               test: /\.pug$/,
               oneOf: [
                  {
                     resourceQuery: /^\?vue/,
                     use: ['pug-plain-loader']
                  },
                  {
                     use: ['raw-loader', 'pug-plain-loader']
                  }
               ]
            }
         ]
      }
   });

mix.sourceMaps();
mix.disableNotifications();
